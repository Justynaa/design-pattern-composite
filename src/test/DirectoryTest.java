package test;

import main.Directory;
import main.File;
import main.FileSystem;
import org.junit.Assert;

import static org.junit.Assert.*;

/**
 * Created by Justyna on 2017-02-16.
 */
public class DirectoryTest {

    @org.junit.Test
    public void getName() throws Exception {
        FileSystem d1 = new File("D1", 5);
        assertEquals("D1", d1.getName());
    }

    @org.junit.Test
    public void testGetSize() throws Exception {
        //DirectoryTest test = new DirectoryTest();
        FileSystem f1 = new File("F1", 5);
        FileSystem f2 = new File("F1", 5);
        FileSystem d1 = new Directory("D1", 1);
        d1.add(f1);
        d1.add(f2);
        FileSystem rootDirectory = new Directory("Root Directory", 1);
        FileSystem f3 = new File("F3", 5);
        rootDirectory.add(d1);
        rootDirectory.add(f1);

        assertEquals(17, rootDirectory.getSize(), 0);
    }

    @org.junit.Test
    public void testGetChild () throws Exception {
        FileSystem f1 = new File("F1", 5);
        FileSystem d1 = new Directory("D1", 1);
        d1.add(f1);

        assertEquals("F1", d1.getChild(0).getName());
    }

}