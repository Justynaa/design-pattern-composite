package main;

/**
 * Created by Justyna on 2017-02-16.
 */
public class FileSystemMain {
    public static void main (String [] args){
        FileSystem f1 = new File("file one", 5);
        FileSystem f2 = new File("file two", 6);
        FileSystem d1 = new Directory("main.Directory one", 1);
        d1.add(f1);
        d1.add(f2);
        FileSystem rootDirectory = new Directory("Root directory", 1);
        FileSystem f3 = new File("file three", 20);

        rootDirectory.add(d1);
        rootDirectory.add(f3);

        //creating directory which I will add to main.Directory one
        FileSystem f4 = new File("file four", 5);
        FileSystem f5 = new File("file five", 5);
        FileSystem d2 = new Directory("main.Directory two", 1);
        d2.add(f4);
        d2.add(f5);
        d1.add(d2);

        rootDirectory.print();
        System.out.println("Total size: " + rootDirectory.getSize());

    }
}
