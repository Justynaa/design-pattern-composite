package main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Justyna on 2017-02-16.
 */
public class Directory implements FileSystem {
    private String name;
    private double size;

    public Directory(String name, double size) {
        this.name = name;
        this.size = size;
    }

    List<FileSystem> files = new ArrayList<FileSystem>();

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getSize(){
        double filesSize = this.size;
        //double filesSize = 0; //wtedy nie uwzdlędniamy rozmiaru samego folderu


        for (FileSystem file : files){
            filesSize += file.getSize();
        }
        return filesSize;
    }

    @Override
    public void add(FileSystem fileSystem) {
        this.files.add(fileSystem);
    }

    @Override
    public void remove(FileSystem fileSystem) {
        this.files.remove(fileSystem);
    }

    @Override
    public void print() {
        System.out.println("main.Directory name: " + getName() + ", size: " + getSize());

        Iterator filesIterator = (Iterator) files.iterator();

        for(FileSystem file : files){
            file.print();
        }
    }

    @Override
    public FileSystem getChild(int i) {
        return this.files.get(i);
    }
}
