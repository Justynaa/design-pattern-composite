package main;

/**
 * Created by Justyna on 2017-02-16.
 */
public class File implements FileSystem {
    private String name;
    private double size;

    public File(String name, double size){
        this.name = name;
        this.size = size;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getSize(){
        return this.size;
    }

    @Override
    public void add(FileSystem fileSystem) {
        //this is leaf node so this method is not applicable to this class.
    }

    @Override
    public void remove(FileSystem fileSystem) {
        //this is leaf node so this method is not applicable to this class.
    }

    @Override
    public void print() {
        System.out.println("main.File name: " + getName() + ", size: " + getSize());
    }

    @Override
    public FileSystem getChild(int i) {
        //this is leaf node so this method is not applicable to this class.
        return null;
    }
}
