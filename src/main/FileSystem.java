package main;

/**
 * Created by Justyna on 2017-02-16.
 */
public interface FileSystem {
    public String getName();
    public double getSize();
    public void add(FileSystem fileSystem);
    public void remove(FileSystem fileSystem);
    public void print();
    public FileSystem getChild(int i);
}
